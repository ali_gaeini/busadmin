import React , { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { Redirect } from 'react-router-dom';
import AddBusModal from '../components/AddBusModal'
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  addIcon : {
    cursor : 'pointer'
  }
}));
const Header = () =>  {
  const classes = useStyles();
  const [ redirect , setRedirect ] = useState( false )
  const logOut = () => {
      localStorage.removeItem("login")
      setRedirect( true )
  }
  if( redirect ){
    return <Redirect to="/"/>
  }else{
    return (
        <div className={classes.root}>
          <AppBar position="static">
            <Toolbar>
              <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={ logOut }>
                <ExitToAppIcon />
              </IconButton>
              <Typography variant="h6" className={classes.title}>
                BusStation
              </Typography>
              <AddBusModal />
            </Toolbar>
          </AppBar>
        </div>
          );
  }
}
export default Header
