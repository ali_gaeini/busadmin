import React from 'react';
import { makeStyles , createTheme , ThemeProvider } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  }
}));
const theme = createTheme({
    direction: 'rtl',
  });
export default function NativeSelects() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    age: '',
    name: 'hai',
  });

  const handleChange = (event) => {
    const name = event.target.name;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  return (
    <div>
        <ThemeProvider theme={theme}>
            <div dir="rtl">
            <FormControl className={classes.formControl}>
                <InputLabel htmlFor="age-native-simple"> <span> خط اتوبوس </span> </InputLabel>
                <Select
                  native
                  value={state.age}
                  onChange={handleChange}
                  inputProps={{
                    name: 'age',
                    id: 'age-native-simple',
                  }}
                >
                  <option aria-label="None" value="" />
                  <option className="options" value={10}>ده</option>
                  <option className="options" value={20}>بیست</option>
                  <option className="options" value={30}>سی</option>
                </Select>
            </FormControl>
            </div>
        </ThemeProvider>
      
    </div>
  );
}
