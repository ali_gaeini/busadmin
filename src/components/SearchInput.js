import React , { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import booksContext from '../context/Books';
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

const SearchInput = () =>  {
  const classes = useStyles();
  const { books , setFilteredBooks } = useContext(booksContext)
  const filter = ( { target } ) => {
    setFilteredBooks( books.filter( item => item.title.includes( target.value )) )
  }
  return (
    <form className={classes.root} noValidate autoComplete="off">
      <TextField label="Title" variant="outlined" onChange={filter}/>
    </form>
  );
}
export default SearchInput