import React , { useState , useContext } from "react";
import Checkbox from "@material-ui/core/Checkbox";
import InputLabel from "@material-ui/core/InputLabel";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { MenuProps, useStyles } from "./MultiSelectUtils";
import booksContext from "../context/Books";
const MultiSelect = () => {
  const classes = useStyles();
  const [selected, setSelected] = useState([]);
  const { books , setFilteredBooks } = useContext(booksContext)
  const tags = books.length ? books[0].tags : []
  const handleChange = (event) => {
    const value = event.target.value;
    if (value[value.length - 1] === "all") {
      setSelected(selected.length === tags.length ? [] : tags);
      return;
    }
    setSelected(value);
    setFilteredBooks( books.filter( item => item.tags ? value.every(r => item.tags.includes(r)) : false ) )
  };

  return (
    <FormControl className={classes.formControl}>
      <InputLabel>Tags</InputLabel>
      <Select
        multiple
        value={selected}
        onChange={handleChange}
        renderValue={(selected) => selected.join(", ")}
        MenuProps={MenuProps}
      >
        {tags.map(( item , index ) => (
          <MenuItem key={index} value={item}>
            <ListItemIcon>
              <Checkbox checked={selected.indexOf(item) > -1} />
            </ListItemIcon>
            <ListItemText primary={item} />
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}

export default MultiSelect;