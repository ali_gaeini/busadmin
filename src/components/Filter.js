import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import SearchInput from './SearchInput';
import MultiSelect from './MultiSelect';
const useStyles = makeStyles((theme) => ({
    container: {
      display : "flex" , 
      flexDirection : "row" , 
      margin: theme.spacing(1)
    }
  }));
const Filter = () => {
    const classes = useStyles();
    return(
        <div className={classes.container}> 
            <SearchInput />
            <MultiSelect />
        </div>
    )
}
export default Filter