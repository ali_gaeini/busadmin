import { Player } from '@lottiefiles/react-lottie-player';
import NotFoundJson from '../json/NotFound.json'
const NotFound = () => {
    return (
        <div>
            <Player
                autoplay
                loop
                src={NotFoundJson}
            >
            </Player>
        </div>
    )
}
export default NotFound