import axios from '../constants/Axios'
const getBooks = async () => {
    let { data } = await axios.get('/books')
    return data.payload
}
const login = async ( username , password ) => {
    let { data } = await axios.post("/auth/login" , { username , password })
    return data.payload
}
const addBusInformation = async ( data ) => {
    let response = await axios.post("/settings" , data )
    return response
}
const getBusInformations = async (  ) => {
    let response = await axios.get("/settings" )
    return response
}
const getBusInformation = async ( id ) => {
    let response = await axios.get(`/settings/${id}` )
    return response
}
const updateBusInformation = async ( id  , data ) => {
    let response = await axios.put(`/settings/${id}` , data )
    return response
}
const deleteItem = async ( id ) => {
    let response = await axios.delete(`/settings/${id}` )
    return response
}
export { getBooks , login , addBusInformation , getBusInformations , deleteItem , getBusInformation , updateBusInformation }