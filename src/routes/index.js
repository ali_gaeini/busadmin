import routes from '../constants/Routes'
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'
const Routes = () => {
    return (
        <Router>
                <div>
                    <Switch>
                        { routes.map(( { path , component } , index ) => 
                            <Route exact path={path} component={component} key={index}/>
                        )}
                    </Switch>
                </div>
            </Router>
    )
}
export default Routes