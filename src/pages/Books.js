import { Redirect } from 'react-router-dom';
import { useContext } from 'react'
import useGetBooks from '../hooks/useGetBooks'
import BooksTable from '../components/BooksTable';
import Filter from '../components/Filter';
import Header from '../components/Header';
import Switcher from '../components/Switcher';
import { makeStyles } from '@material-ui/core/styles';
import BooksCard from '../components/BooksCard';
import booksContext from '../context/Books';
const useStyles = makeStyles((theme) => ({
    container : {
        display : "flex" , 
        flexDirection : "row" , 
        justifyContent : "space-between" , 
    } , 
    switcher : {
        maxWidth :"50%" , 
        display : "flex" , 
        alignItems : "center" , 
        marginRight : theme.spacing(3)
    }
  }));
const Books = () => { 
    const classes = useStyles();
    const { ShowType } = useContext(booksContext)
    useGetBooks()
    if( !localStorage.getItem("login") || localStorage.getItem("login") === false ){
        return <Redirect to="/"/>
    } else {
        return (
            <div>
                <Header />
                <div className={classes.container}>
                    <Filter />
                    <div className={classes.switcher} > <Switcher/> </div>
                </div>
                { ShowType === "Table" ? <BooksTable /> : <BooksCard /> }    
            </div>
        )
    }
}
export default Books