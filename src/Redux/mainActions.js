const setCards = (value) => {
  return {
    type: 'SET-CARDS',
    value,
  }
}
const setCode = (value) => {
  return {
    type: 'SET_CODE',
    value,
  }
}
const setStationName = (value) => {
  return {
    type: 'SET_STATION_NAME',
    value,
  }
}
const setImage = (value) => {
  return {
    type: 'SET_IMAGE',
    value,
  }
}
const setCardPrice = (value) => {
  return {
    type: 'SET_CARD_PRICE',
    value,
  }
}
const setCashPrice = (value) => {
  return {
    type: 'SET_CASH_PRICE',
    value,
  }
}
const setStartTime = (value) => {
  return {
    type: 'SET_START_TIME',
    value,
  }
}
const setBusInformations = (value) => {
  return {
    type: 'SET_BUS_INFORMATIONS',
    value,
  }
}

export { setCards , setCode , setStationName , setImage , setCardPrice , setCashPrice ,setStartTime , setBusInformations}
