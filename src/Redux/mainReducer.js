const initialState = {
  cards: [],
  code : '' , 
  station_name : '' ,
  image : '' , 
  card_price : ''  ,
  cash_price : '' , 
  start_time : '' , 
  busInformations : [] 
}
const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET-CARDS':
      return { ...state, cards: action.value }
    case 'SET_CODE' : 
      return { ...state, code: action.value }
    case 'SET_STATION_NAME' : 
      return { ...state, station_name: action.value }
    case 'SET_IMAGE' : 
      return { ...state, image: action.value }
    case 'SET_CARD_PRICE' : 
      return { ...state, card_price: action.value }
    case 'SET_CASH_PRICE' : 
      return { ...state, cash_price: action.value }
    case 'SET_START_TIME' : 
      return { ...state, start_time: action.value }
    case 'SET_BUS_INFORMATIONS' : 
      return { ...state, busInformations: action.value }
    default:
      return state
  }
}
export default mainReducer
